"use strict";

function password() {
  const random = Math.floor(Math.random() * 101);
  let intentos = 1;

  do {
    let answer = parseInt(prompt("Introduzca un numero del 0 al 100"));
    if (answer === random) break;
    else if (intentos === 5) break;
    intentos++;
    alert(answer > random ? "Es MENOR" : "Es MAYOR");
  } while (true);
  {
    if (intentos === 5) {
      alert("Has perdido");
    } else {
      alert(`Has ganado, la contraseña era ${random}`);
    }
  }
}

password();
