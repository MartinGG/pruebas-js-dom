"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];
let points = [];
let total = 0;
function ordenar() {
  for (const { equipo: e, puntos: p } of puntuaciones) {
    total = p.reduce((a, b) => a + b, 0);
    points.push({ e, total });
  }
  points.sort((a, b) => b.total - a.total);
  console.log(
    `*** El equipo con más puntos es: ${points[0].e}, con un total de: ${
      points[0].total
    } ***
*** El equipo con menos puntos es: ${
      points[points.length - 1].e
    }, con un total de: ${points[points.length - 1].total} ***`
  );
}

ordenar();
