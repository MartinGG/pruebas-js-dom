"use strict";

async function cargar() {
  const body = document.querySelector("body");
  const create = document.createElement("div");
  body.appendChild(create);
}

async function hora() {
  const date = new Date();
  let hora = date.getHours();
  let minutos = date.getMinutes();
  let segundos = date.getSeconds();
  if (hora < 10) {
    hora = `0${hora}`;
  }
  if (minutos < 10) {
    minutos = `0${minutos}`;
  }
  if (segundos < 10) {
    segundos = `0${segundos}`;
  }
  const reloj = `${hora} : ${minutos} : ${segundos}`;
  const div = document.querySelector("div");
  div.innerHTML = `${reloj}`;

  return reloj;
}

cargar().then(hora);
setInterval(() => {
  hora();
}, 1000);
